import { url, root } from "./index.js";

async function getData(url) {
    try{
        const response = await fetch(url);
        const data = await response.json();
        return data;
    }
        catch(err) {
            console.error(err)
        }  
}
function getAddress(ip) {
    return `http://ip-api.com/json/${ip}?fields=status,message,continent,country,regionName,city,district`
}
export async function findByIP(){
    try{root.innerHTML = '';
        insertSpinner(root);
        const ip = await getData(url);
        console.log(ip);
        const data = await getData(getAddress(ip.ip));
        console.log(data);
        showAddress(data)
    }
    catch (err){
        console.error(err)
    }
}

 function showAddress({continent, country, regionName, city, district}) {
    root.innerHTML = `
        <p>You are on the continent <strong>${continent}</strong> </p>
        <p>In the counry <strong>${country}</strong></p>
        <p>Your city <strong>${city}</strong> in region <strong>${regionName}</strong></p>
        ${district ? `<p>District <strong>${district}</strong></p>` : ''}
    `
}

function insertSpinner(insertIn) {
    insertIn.insertAdjacentHTML('afterbegin', `
    <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
    `)
}


