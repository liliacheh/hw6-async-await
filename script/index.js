import { findByIP } from "./functions.js";

export const root = document.querySelector('.root')
const btn = document.querySelector(".btn");
export const url = 'https://api.ipify.org/?format=json';

btn.addEventListener("click", findByIP)
